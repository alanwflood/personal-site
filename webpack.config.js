const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const Path = require('path');
const {
  getIfUtils,
  removeEmpty
} = require('webpack-config-utils');

module.exports = (env) => {
  const {
    ifDevelopment,
    ifProduction
  } = getIfUtils(env);
  return {
    entry: Path.join(__dirname, 'src/index.js'),
    output: {
      path: Path.join(__dirname, 'dist'),
      publicPath: '/',
      filename: 'bundle.js',
    },
    module: {
      rules: [{
        test: /\.jsx?/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.md$/,
        loader: "html-loader"
      },
      {
        test: /\.(jpg|png|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[hash].[ext]',
        },
      }],
    },
    devServer: ifDevelopment({
      port: 8001,
      inline: true,
      historyApiFallback: true,
      open: true,
      overlay: {
        errors: true,
      },
    }),
    devtool: 'eval-source-map',
    plugins: removeEmpty([
      new HTMLWebpackPlugin({
        template: Path.join(__dirname, 'index.html'),
      }),
      ifProduction(new webpack.optimize.UglifyJsPlugin()),
    ]),
    resolve: {
      extensions: ['.js', '.jsx'],
    },
  };
};
