import React from "react";
import Styled, { injectGlobal } from "styled-components";
import { normalize } from "polished";
import ReactGA from "react-ga";

import Hero from "./components/hero";
import About from "./components/about";
import Portfolio from "./components/portfolio";
import Music from "./components/music";

injectGlobal`${normalize()}`;

ReactGA.initialize(process.env.GA_TRACKING_ID);

export default () => (
  <div style={{ backgroundColor: "#232323" }}>
    <Hero />
    <About />
    <Portfolio />
    <Music />
  </div>
);
