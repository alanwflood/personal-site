import React from "react";
import fetch from 'isomorphic-unfetch';
import Styled from 'styled-components';

const LastFm = Styled.div`
  color: white;
  max-width: 62.5rem;
  margin: 2em auto 0;
  font-family: 'Space Mono', 'monospace';

  .loading {
    text-align: center;
    padding-top: 2em;
  }

  a {
    color: #fff;
    text-decoration: none;

    &:focus,
    &:active,
    &:hover {
      text-decoration: underline;
    }
  }

  .song-details {
    margin: 0 auto;
    border-top: 2px solid white;
    padding-top: 2em;
    padding-bottom: 2em;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
      margin-right: 1em
    }
  }
`;

export default class LastFM extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listeningTo: false
    }
  }

  componentDidMount() {
    fetch("https://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=alcandrive55&api_key=47864f65e543ea1bf989daf32045a901&format=json")
      .then(res => res.json())
      .then(success => {
        const { album, artist, date, name, image } = success.recenttracks.track[0]

        this.setState({
          listeningTo: {
            date: date.uts,
            track: name,
            album: album["#text"],
            artist: artist["#text"],
            image: image[3]["#text"]
          }
        });
      })
      .catch(error => console.error(error))
  }

  render() {
    const {date, track, album, artist, image} = this.state.listeningTo;
    const currentTime = Math.round((new Date()).getTime() / 1000);
    if (this.state.listeningTo) {
      return (
        <LastFm>
          <h2>Some Call Me a Music Connosieur</h2>
          <h4>Here's what I've been listening to:</h4>
          <div className="song-details">
            <img src={image} alt={`${album} by ${artist}`} />
            <p>
              <a href="https://www.last.fm/user/AlCanDrive55" target="_blank" rel="noopener noreferrer">
                {artist} - {track}<br/>
                <small>{album}</small>
              </a>
              <br/>
              <br/>
              <br/>
              Follow my spotify playlists <a href="" target="_blank" rel="noopener noreferrer">here</a>.
            </p>
          </div>
        </LastFm>
      );
    } else {
      return (
        <LastFm>
          <div className="loading">
            <i className="fa fa-circle-o-notch fa-spin fa-3x" />
          </div>
        </LastFm>
      );
    }
  }
}
