import React from "react";
import Styled from "styled-components";
import { Link } from 'react-router-dom';

const Card = Styled.a`
  width: 100%;
  color: #373d3f;
  text-decoration: none;
  box-sizing: border-box;
  padding: 1rem 2rem;
  margin: 1em auto;
  border: 2px solid #f1f1f1;
  border-radius: 10px;
  background-image: linear-gradient(90deg, #fff, #fff);
  font-family: 'Space Mono', 'monospace';
  transform: transitionY(0);
  transition: transform .3s ease-in-out, background-image 5s ease-in-out;

  .card-interior {
    max-width: 62.5rem;
    width: 100%;
    margin: 0 auto;
    display: flex;
    align-items: center;
    flex-flow: column;
    text-align: initial;

    @media(min-width: 32.5rem) {
      flex-flow: row;
      text-align: auto;
    }
  }

  .job {
    font-size: 1.6rem;
    font-weight: 500;
  }

  .position {

  }
  .place {}
  .date {}

  &:hover,
  &:focus,
  &:active {
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.50);
    color: white;
    transform: translateY(-2px);

    &.gradient-blue {
      background-image: linear-gradient(45deg, #2e3192, #1bffff);
    }

    &.gradient-orange {
      background-image: linear-gradient(45deg, #d4145a, #fbb03b);
    }

    &.gradient-purple {
      background-image: linear-gradient(45deg, #662d8c, #ed1e79);
    }

    &.gradient-magenta {
      background-image: linear-gradient(45deg, #8e78ff, #ff7d78);
    }

    &.gradient-green {
      background-image: linear-gradient(45deg, #009e00, #ffff96);
    }

    &.gradient-seafoam {
      background-image: linear-gradient(45deg, #00a8c5, #ffff7e);
    }

    &.gradient-light-blue {
      background-image: linear-gradient(45deg, #3a3897, #a3a1ff);
    }
  }

  div {
    width: 100%;
  }

`;

export default (props) => (
  <Card href={props.job.url} target="_blank" rel="noopener noreferrer" className={`gradient-${props.job.color}`}>
    <div className="card-interior">
      <div>
        <span className="job">{props.job.place}</span><br/>
        <span className="position">{props.job.position}</span>
      </div>
      <div style={{textAlign: 'center'}} className="place">
        {props.job.work}
      </div>
      <div style={{textAlign: 'right'}} className="date">
        {props.job.date}
      </div>
    </div>
  </Card>
)
