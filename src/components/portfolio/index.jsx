import React from "react";
import { BrowserRouter, Route } from 'react-router-dom'
import Styled from "styled-components";

import PortfolioData from "../../siteInfo.json";
import Index from "./portfolioIndex";
import Show from "./portfolioShow";
import Card from "./card";

const Portfolio = Styled.div`
  max-width: 62.5rem;
  margin: 0 auto;

  h2 {
    width: 100%;
    margin-left: 1em;
    font-family: 'Space Mono', 'monospace';
    color: #fff;
  }
`;

export default () => (
  <Portfolio id="portfolio">
    {/* <BrowserRouter>
        <div>
        <Route exact path="/" render={() => <Index/>}/>
        { PortfolioData.jobs.map(job => <Route path={`/${job.relativeURL}`} key={job.place} render={() => <Show markdownFile={job.relativeURL}/>} />) }
        </div>
        </BrowserRouter> */}
    <Index />
  </Portfolio>
)
