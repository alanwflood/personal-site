import React from 'react';
import PortfolioData from "../../siteInfo.json";
import Card from "./card";
import Styled from "styled-components";
import { Link } from 'react-router-dom'

const Portfolio = Styled.div`
  display: flex;
  flex-flow: row wrap;
  width: 100%;
  margin: 0 auto;
  padding-bottom: 20vh;
`;

export default () => (
  <Portfolio>
    <h2>Portfolio</h2>
    { PortfolioData.jobs.map(job => <Card job={job} key={job.place} />) }
  </Portfolio>
)

