import React from 'react';
import Marked from 'marked';
import { Link } from 'react-router-dom';
import Styled from 'styled-components';

const Content = Styled.div`
  padding-bottom: 20vh;
  max-width: 32.5rem;
  width: 100%;
  margin: 0 auto;
  padding-top: 1em;

  h1, h2, h3, h4, h5 {
    text-decoration: none !important;
    font-family: 'Space Mono', 'monospace';
  }

  p {
    font-family: 'Space Mono', 'monospace';
  }

  h1, h2, h3, h4, h5, p, a {
    color: #fff !important;
  }
`

export default class PortfolioShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      markdown: false
    }
  }

  componentWillMount() {
    const data = require(`./markdown/${this.props.markdownFile}.md`);
    this.setState({
      markdown: Marked(data)
    })
  }

  render() {
    const { markdown } = this.state;

    return (
      <div style={{backgroundColor: "#a49694"}}>
        <Content>
          <Link to="/"><i className="fa fa-chevron-left"/>Back to index</Link>
          <article dangerouslySetInnerHTML={{__html: markdown}} />
        </Content>
      </div>
    )
  }
}
