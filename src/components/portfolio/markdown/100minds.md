# 100minds

100minds is a social enterprise founded in 2013, that brings together some of Ireland’s top college students and connects them with one cause: To achieve big goals in a short space of time.

100minds recruit proactive college students from across the country and challenge them to raise €1,000 each in a creative and entrepreneurial way. Collectively, we set ourselves the goal of generating a large donation for a worthy charity in a lean and transparent way. 
