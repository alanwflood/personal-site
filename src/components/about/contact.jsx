import React from "react";
import Styled from "styled-components";
import socialMedia from "../../siteInfo.json";

const Contact = Styled.div`
  width: 100%;
  padding: 2rem 0;

  h3 {
    max-width: 32.5rem;
    margin: 0 auto 2em;
    color: #fff;
    font-family: 'Space Mono', 'monospace';
  }

  ul {
    max-width: 32.5rem;
    align-items: center;
    justify-content: center;
    list-style-type: none;
    display: flex;
    margin: 0 auto;
    padding: 0;
  }

  .social-link {
    color: #fff;
    padding: .5em .5em;
    font-size: 2rem;
    transition: color .4s ease-in-out;

    @media(min-width: 32.5rem) {
      margin: .5em .5em;
    }

    &:active,
    &:focus,
    &:hover {
      color: #90FDB9;
    }

    &:visited {
      color: #f1f1f1;

    }
  }
`;

const SocialMediaLinks = socialMedia.socialNetworks.map(sm => {
    if (sm.hidden) return;
    return (
      <li key={sm.name}>
        <a className="social-link" href={sm.url} rel="noopener noreferrer" target="_blank">
          <i className={`fa fa-${sm.icon}`} aria-label={!!sm.alt ? sm.alt : `Alan's ${sm.name} profile`}/>
        </a>
      </li>
    )
  }
);



export default () => (
  <Contact id="contact">
    <h3>Let's Keep In Touch:</h3>
    <ul>
      {SocialMediaLinks}
    </ul>
  </Contact>
)
