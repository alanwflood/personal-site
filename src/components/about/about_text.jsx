import React from "react";
import Waypoint from "react-waypoint";
import Styled from "styled-components";
import Anime from "animejs";
import siteText from "../../siteInfo.json";
import me from "../../assets/me.jpg";
import hand from "../../assets/wave.png";
import Contact from "./contact";

const Avatar = Styled.div`
  border-radius: 999px;
  max-width: 15em;
  position: relative;
  overflow: hidden;
  margin: 0 auto;
  width: 100%;
  border: 6px solid #f1f1f1;

  &::before {
    content: "";
    display: block;
    padding-bottom: 100%;
  }

  img {
    display: block;
    position: absolute;
    top: 0;
    width: 100%;
    height: 100%;
  }
`;

const AboutContainer = Styled.div`
  display: flex;
  flex-flow: column;
  height: 100vh;
  justify-content: center;
`;

const About = Styled.article`
  max-width: 35rem;
  padding: 0 1em;
  margin: 0 auto;

  h2 {
    font-family: 'Space Mono', 'monospace';
    color: #fff;

    img {
      display: inline-block;
      height: 2.5rem;
    }
  }

  p {
    font-family: 'Space Mono', 'monospace';
    color: #fff;
  }

  hr {
    border-color: white;
    border-style: solid
  }

  strong {
    display: block;
    font-weight: bold;
    margin: 1em 2em 0;

    @media(max-width: 32.5rem) {
      text-align: center;
    }
  }
`;

export default class AboutMe extends React.Component {
  componentDidMount() {
    this.animation = Anime({
      targets: "#hand",
      rotate: ["0deg", "20deg", "0deg"],
      easing: "easeOutExpo",
      autoplay: false,
      loop: 4
    });
  }

  render() {
    return (
      <AboutContainer id="about">
        <Avatar>
          <img src={me} alt="Alan Flood" />
        </Avatar>
        <About>
          <Waypoint onEnter={() => this.animation.restart()} >
            <h2>
              Hi, I'm Alan, <img src={hand} alt="hand waving emoji" id="hand"/>
            </h2>
          </Waypoint>
          <p>
            I'm a Web Developer working in the industry since 2014. During that time I've worked with a variety of teams on a myriad of projects and products.
          </p>
          <p>
            I always strive to practice and learn new things and I love working on interesting projects with cool people.
          </p>
          <hr/>
        </About>
        <Contact />
      </AboutContainer>
    )
  }
}
