import React from "react";
import Styled from 'styled-components';
import Anime from 'animejs';
import Terminal from './terminal'

const Hero = Styled.div`
align-items: center;
display: flex;
height: 100vh;
justify-content: center;
width: 100%;
overflow: hidden;
position: relative;
`;


export default class NewHero extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      typing: false
    };
  }

  animateText = () => {
    const animation = Anime.timeline();
    animation.add({
      duration: 2000,
      targets: "#terminal",
      opacity: [0,1],
      complete: () => {
        this.setState({typing: true});
      }
    })
  };

  componentDidMount() {
    this.animateText();
  };

  render() {
    return (
      <Hero id="hero">
        <Terminal id="terminal" typing={this.state.typing} />
      </Hero>
    );
  }
}
