import React from "react";
import Styled from 'styled-components';
import Typist from 'react-typist';
import siteText from '../../siteInfo.json';

const Terminal = Styled.div`
background-color: #565656;
border-radius: 5px;
box-shadow: 0 2px 4px 0 rgba(0,0,0,0.50);
height: 70vh;
margin: 0 1em;
max-height: 30rem;
max-width: 62.5rem;
min-height: 30rem;
opacity: 0;
overflow: hidden;
width: 100%;
z-index: 3;


.Typist {
  .Cursor {
    display: inline-block;
  }

  .Cursor--blinking {
    opacity: 1;
    animation: blink 1s linear infinite;
    @keyframes blink {
      0% { opacity:1; }
      50% { opacity:0; }
      100% { opacity:1; }
    }
  }
}

.topbar {
  background-color: #cfcfc4;
  width: 100%;
  height: 2rem;
  display: flex;
  position: relative;
}

.buttons {
  display: flex;
  height: 100%;
  max-width: 100px;
  align-items: center;
  padding: 0 5px;
  position: absolute;
  top: 0;

  div {
    margin: 0 5px;
    height: 15px;
    width: 15px;
    border-radius: 999px;
  }

  div:first-child {
    background-color: red;
  }


  div:nth-child(2) {
    background-color: yellow;
  }

  div:nth-child(3) {
    background-color: green;
  }
}

.titlebar {
  height: 100%;
  display: flex;
  text-align: center;
  align-items: center;
  margin: 0 auto;
}

.terminal-content {
  font-family: 'Space Mono', 'monospace';
  padding: 1rem;
  color: #90FDB9;
  font-size: 1.4rem;

  @media(min-width: 32.5rem) {
    font-size: 2rem;
  }

  .name {
    color: #f24645;
  }

  a {
    color: #b066fe;
  }

  .defiant-tricolor {
    color: #fff;

    .green {
      color: #009A49;
    }
    .orange {
      color: #FF7900;
    }
  }

  ul li {
    cursor: pointer;

    &:hover {
      text-decoration: underline;
    }
  }
}
`;

function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }
  return array;
}

export default class TerminalType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      typing: props.typing,
    };
  }

  componentWillReceiveProps(newProps) {
    this.setState({typing: newProps.typing});
  }

  typingLoop = () => {
    this.setState({ typing: false }, () => {
      this.setState({ typing: true })
    });
  }

  scrollTo = (el) => {
    document.querySelector(el).scrollIntoView({block: 'end', behavior: 'smooth'});
  }

  render() {
    return (
      <Terminal id={this.props.id}>
        <div className="topbar">
          <div className="buttons">
            <div></div>
            <div></div>
            <div></div>
          </div>
          <div className="titlebar">
            <div>Oh-My-Gosh[Shell]</div>
          </div>
        </div>
        <div className="terminal-content">
          <div style={{minHeight: "1.4em"}}>
            I'm <a href="mailto:alanwflood@gmail.com" className="name">Alan Flood</a>, a <a href="https://github.com/alanwflood" target="_blank" rel="noopener noreferrer">Developer</a> in <a href="https://minicorp.ie" className="minicorp" target="_blank" rel="noopener noreferrer">Minicorp</a>, living in Dublin, <span className="defiant-tricolor"><span className="green">Ir</span>ela<span className="orange">nd</span></span>
          </div>
          <br/>
          <div style={{minHeight: "1.4em"}}>
            {this.state.typing && <Typist
                                    cursor={{
                                      show: true,
                                      blink: true,
                                      element: '█'
                                    }}
                                    onTypingDone={() => this.typingLoop()}
                                    avgTypingDelay={55} >
              <span>I <i className="fa fa-heart"/> </span>
              {shuffle(siteText.skills).map(text =>
                <span key={text}>
                  <span>{text}</span>
                  <Typist.Backspace count={text.length} delay={2000} />
                </span>
              )}
            </Typist>}
          </div>
          <div>
            <ul>
              <li onClick={() => this.scrollTo("#about")}>About</li>
              <li onClick={() => this.scrollTo("#portfolio")}>Portfolio</li>
              <li onClick={() => this.scrollTo("#contact")}>Contact</li>
            </ul>
          </div>
        </div>
      </Terminal>
    );
  }
}
